package no.uib.inf101.terminal;

// UiB INF101 ShellLab - Main.java
// Dette er filen som inneholder main-metoden.

public class Main {

  public static void main(String[] args) {
    // Create a new shell
    /* CmdEcho commandEcho = new CmdEcho();
       shell.installCommand(commandEcho); 
       En mindre kompakt måte en måten nedenfor, men 
       de gjør det samme*/

    SimpleShell shell = new SimpleShell();
    shell.installCommand(new CmdEcho());
    shell.installCommand(new CmdExit());

    // Run the shell in the terminal GUI
    Terminal gui = new Terminal(shell);
    gui.run();
  }
}