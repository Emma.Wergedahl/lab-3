package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {
        String text = "";

        for(String s: args) {
            s += " ";
            text += s;
        }
        return text;
    }

    @Override
    public String getName() {
        return "echo";
    }
    
}
